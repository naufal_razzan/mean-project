import { Injectable } from '@angular/core';
import { WebRequestService } from './web-request.service';
import { List } from './modules/list.module';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private webReqService: WebRequestService) { }

  getList(){
    return this.webReqService.get('lists')
  }

  createList(title: string){ // web request to create new list
    return this.webReqService.post('lists', { title })
  }

  getTasks(listId: String){
    return this.webReqService.get(`lists/${listId}/tasks`)
  }

  createTask(title: string, listId: String){
    return this.webReqService.post(`lists/${listId}/tasks`, { title })
  }
}

