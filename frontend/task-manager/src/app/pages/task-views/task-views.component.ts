import { Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TaskService } from 'src/app/task.service';

@Component({
  selector: 'app-task-views',
  templateUrl: './task-views.component.html',
  styleUrls: ['./task-views.component.scss']
})
export class TaskViewsComponent {

  lists: any[];
  tasks: any[];
  constructor(private taskService: TaskService, private route: ActivatedRoute){

  }

  ngOnInit(){
    this.route.params.subscribe((params: Params) => {
      console.log(params)
      this.taskService.getTasks(params.listId).subscribe((tasks: any) => {
        this.tasks = tasks
      })
    })

    this.taskService.getList().subscribe((lists: any) => {
      this.lists = lists
    })
  }

}
